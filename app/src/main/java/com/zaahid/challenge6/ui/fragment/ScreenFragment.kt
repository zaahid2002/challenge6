package com.zaahid.challenge6.ui.fragment

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.zaahid.challenge6.R
import com.zaahid.challenge6.ui.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class ScreenFragment : Fragment() {

    private val userViewModel :UserViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userViewModel.getLang().observe(viewLifecycleOwner) {
            if (it.isNullOrEmpty()) {
                userViewModel.setLang("en-US")
                setLocate("en-US")
            }else{
                setLocate(it)
            }
        }
        cekstatus()
    }
    private fun cekstatus() {
        userViewModel.getUserName().observe(viewLifecycleOwner){
            if(it.isNullOrEmpty().not()){
                findNavController().navigate(R.id.action_screenFragment_to_homeActivity)
                Toast.makeText(requireContext(), getString(R.string.welcome_back)+" "+it+" ",Toast.LENGTH_LONG).show()
            }else{
                findNavController().navigate(R.id.action_screenFragment_to_loginFragment)
            }
        }
    }
    private fun setLocate(Lang: String) {
        val locale = Locale(Lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.setLocale(locale)
        requireContext().resources.updateConfiguration(config,requireContext().resources.displayMetrics)
    }
}