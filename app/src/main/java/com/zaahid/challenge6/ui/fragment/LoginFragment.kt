package com.zaahid.challenge6.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.zaahid.challenge6.R
import com.zaahid.challenge6.data.local.database.entity.UserEntity
import com.zaahid.challenge6.databinding.FragmentLoginBinding
import com.zaahid.challenge6.ui.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding
    private val viewModel: UserViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonLogin.setOnClickListener {
            cekForm()
        }
        binding.toregister.setOnClickListener {
            toRegister()
        }
    }

    fun toRegister() {
        binding.toregister.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

    fun cekForm() {
        if (binding.inputusename.text.toString().isEmpty().not()) {
            if (binding.textinputpassword.text.toString().isEmpty().not()) {
                viewModel.getUserByUsername(binding.inputusename.text.toString().trim())
                viewModel.detailDataResult.observe(viewLifecycleOwner) {
                    login(it.payload)
                }
            } else Toast.makeText(
                requireContext(),
                getString(R.string.password_is_empty),
                Toast.LENGTH_SHORT
            ).show()
        } else Toast.makeText(
            requireContext(),
            getString(R.string.username_is_empty),
            Toast.LENGTH_SHORT
        ).show()
    }

    fun login(user: UserEntity?) {
        if (user != null) {
            if (user.username.isEmpty().not()) {
                viewModel.setUserPref(
                    user.userId, user.username, user.useremail.toString(),
                    user.alamat.toString()
                )
                Toast.makeText(
                    requireContext(),
                    user.username + " " + getString(R.string.successfully_logged_in),
                    Toast.LENGTH_SHORT
                ).show()
                findNavController().navigate(R.id.action_loginFragment_to_homeActivity)
            } else Toast.makeText(
                requireContext(),
                getString(R.string.failed_login),
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}