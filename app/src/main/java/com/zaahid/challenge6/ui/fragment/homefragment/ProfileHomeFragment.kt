package com.zaahid.challenge6.ui.fragment.homefragment

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.work.WorkInfo
import com.zaahid.challenge6.R
import com.zaahid.challenge6.data.local.database.entity.UserEntity
import com.zaahid.challenge6.databinding.FragmentProfileHomeBinding
import com.zaahid.challenge6.ui.MainActivity
import com.zaahid.challenge6.ui.UserViewModel
import com.zaahid.challenge6.utils.Utils
import com.zaahid.challenge6.worker.KEY_IMAGE_URI
import dagger.hilt.android.AndroidEntryPoint
import java.io.File
import java.util.*

const val REQUEST_CODE_PERMISSION = 201

@AndroidEntryPoint
class ProfileHomeFragment : Fragment() {

    private var _binding: FragmentProfileHomeBinding? = null
    private val binding get() = _binding!!
    private val userViewModel: UserViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentProfileHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enabledEdit()
        binding.editButton.setOnClickListener {
            cekForm()
        }
        binding.logoutButton.setOnClickListener {
            userViewModel.setUserId(-1)
            val intent = Intent(context, MainActivity::class.java)
            Toast.makeText(context, "Good Bye", Toast.LENGTH_LONG).show()
            startActivity(intent)
        }
        binding.changeLanguage.setOnClickListener {
            showChangeLang()
        }
        binding.buttonUpload.setOnClickListener {
            checkingPermission()
            chooseImageDialog()
        }
        userViewModel.getImageString().observe(viewLifecycleOwner){
            if (it.isNullOrEmpty().not()){
                val bitmap :Bitmap = Utils.convertStringToBitmap(it)
                binding.imageProfile.setImageBitmap(bitmap)
                binding.imagedelete.visibility = View.VISIBLE
            }else{
                binding.imageProfile.setImageResource(R.drawable.ic_account_box)
                binding.imagedelete.visibility = View.GONE
            }
        }
        binding.imagedelete.setOnClickListener {
            userViewModel.setImageString("")
            userViewModel.setImageUri(null)
            userViewModel.setOutputUri(null)
        }
        userViewModel.outputWorksInfos.observe(viewLifecycleOwner,workInfosObserver())
        cekstatus()

    }
    fun relogin(user: UserEntity?) {
        if (user != null) {
            if (user.username.isEmpty().not()) {
                userViewModel.setUserPref(
                    user.userId, user.username, user.useremail.toString(),
                    user.alamat.toString()
                )
                Toast.makeText(
                    requireContext(),
                    user.username + " " + "good",
                    Toast.LENGTH_SHORT
                ).show()
            } else Toast.makeText(
                requireContext(),
                "failed",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onResume() {
        super.onResume()
        cekstatus()
        enabledEdit()
    }
    private fun cekstatus() {
        userViewModel.getUserName().observe(viewLifecycleOwner){
            if(it.isNullOrEmpty()){
                val intent = Intent(requireContext(),MainActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun checkingPermission() {
        if(isGranted(
                requireActivity(), Manifest.permission.CAMERA,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.MANAGE_EXTERNAL_STORAGE
                ), REQUEST_CODE_PERMISSION
            )
        ){}
    }

    private fun chooseImageDialog() {
        AlertDialog.Builder(requireContext())
            .setMessage("Pilih Gambar")
            .setPositiveButton("galeri") { _, _ -> openGalery() }
            .setNegativeButton("Camera") { _, _ -> openCamera() }
            .show()
    }

    private val cameraresult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                handleCameraImage(result.data)
            }
        }

    private fun handleCameraImage(intent: Intent?) {
        val bitmap = intent?.extras?.get("data") as Bitmap
        userViewModel.setImageString(Utils.bitMapToString(bitmap))
    }

    private fun openCamera() {
//        val photoFile = File.createTempFile("IMG_",".JPG",requireContext().getExternalFilesDir(
//            Environment.DIRECTORY_PICTURES))
//        val uri = FileProvider.getUriForFile(requireContext(), requireContext().packageName + ".provider",photoFile)
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraresult.launch(cameraIntent)
    }

    private val galeryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) { result ->
//            binding.imageProfile.setImageURI(result)
            userViewModel.setImageUri(result)
            userViewModel.setImageString(
                Utils.bitMapToString(
                    MediaStore.Images.Media.getBitmap(
                        requireContext().contentResolver,
                        result
                    )
                )
            )
            userViewModel.applyBlur(2)
        }

    private fun openGalery() {
        requireActivity().intent.type = "image/*"
        galeryResult.launch("image/*")
    }
    private fun isGranted(
        activity: Activity,
        permission: String,
        permissions: Array<String>,
        request: Int
    ): Boolean {
        val permissionCheck = ActivityCompat.checkSelfPermission(activity, permission)
        return if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(activity, permissions, request)
            }
            false
        } else {
            true
        }
    }

    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(requireContext())
            .setTitle("Permission Denied")
            .setMessage("Permission is Denied, pleas Allow Permission from settings")
            .setPositiveButton(
                "App Settings"
            ) { _, _ ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
//                val uri = Uri.fromParts("package",packageName,null)
//                intent.data = uri
                startActivity(intent)
            }
            .setNegativeButton("Cancecl") { dialog, _ -> dialog.cancel() }
            .show()
    }

    private fun cekForm() {
        if (binding.textinputusername.text.toString().isEmpty().not()) {
            if (binding.textinputpassword.text.toString().isEmpty().not()) {
                if (binding.inputConfirmPassword.text.toString().isEmpty().not()) {
                    if (binding.textinputalamat.text.toString().isEmpty().not()) {
                        register()
                    } else Toast.makeText(
                        requireContext(),
                        getString(R.string.empty_address),
                        Toast.LENGTH_SHORT
                    ).show()
                } else Toast.makeText(
                    requireContext(),
                    getString(R.string.confirm_pass_empty),
                    Toast.LENGTH_SHORT
                ).show()
            } else Toast.makeText(
                requireContext(),
                getString(R.string.password_is_empty),
                Toast.LENGTH_SHORT
            ).show()
        } else Toast.makeText(
            requireContext(),
            getString(R.string.username_is_empty),
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun register() {
        if (cekPasswordIsSame()) {
            userViewModel.getUserId().observe(viewLifecycleOwner) {
                userViewModel.updateUser(parseFormToEntity(it))
            }
            Toast.makeText(
                requireContext(),
                getString(R.string.successfully_insert_data),
                Toast.LENGTH_LONG
            ).show()
        } else Toast.makeText(
            requireContext(),
            getString(R.string.password_not_same),
            Toast.LENGTH_LONG
        ).show()
    }

    private fun parseFormToEntity(id: Int): UserEntity {
        return UserEntity(
            userId = id,
            username = binding.textinputusername.text.toString().trim(),
            useremail = binding.textinputemail.text.toString().trim(),
            userpassword = binding.textinputpassword.text.toString().trim(),
            alamat = binding.textinputalamat.text.toString().trim(),
            )
    }

    private fun cekPasswordIsSame(): Boolean {
        return binding.textinputpassword.text.toString()
            .trim() == binding.inputConfirmPassword.text.toString().trim()
    }

    private fun enabledEdit() {
        userViewModel.getUserName().observe(viewLifecycleOwner) {
            userViewModel.getUserByUsername(it)
        }
        userViewModel.detailDataResult.observe(viewLifecycleOwner) {
            showEdit(it.payload)
        }
    }

    private fun showEdit(userEntity: UserEntity?) {
        if (userEntity != null) {
            binding.textinputusername.setText(userEntity.username)
            binding.textinputemail.setText(userEntity.useremail)
            binding.textinputpassword.setText(userEntity.userpassword)
            binding.textinputalamat.setText(userEntity.alamat)
        }
    }

    private fun showChangeLang() {
        val listItem = arrayOf("english", "Indonesia", "日本語")
        val mBuilder = AlertDialog.Builder(requireContext())
        mBuilder.setTitle("Choose Language")
        mBuilder.setSingleChoiceItems(listItem, -1) { dialog, which ->
            when (which) {
                0 -> {
                    setLocate("en-US")
                    activity?.recreate()
                }
                1 -> {
                    setLocate("id")
                    activity?.recreate()
                }
                2 -> {
                    setLocate("ja")
                    activity?.recreate()
                }
            }
            dialog.dismiss()
        }
        val mDialog = mBuilder.create()
        mDialog.show()
    }
    private fun setLocate(Lang: String) {
        val locale = Locale(Lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.setLocale(locale)
        requireContext().resources.updateConfiguration(
            config,
            requireContext().resources.displayMetrics
        )
        userViewModel.setLang(Lang)
    }
    private fun workInfosObserver(): androidx.lifecycle.Observer<List<WorkInfo>> {
        return androidx.lifecycle.Observer { listOfWorkInfo ->
            if (listOfWorkInfo.isNullOrEmpty()){
                return@Observer
            }
            val workInfo=listOfWorkInfo[0]
            if (workInfo.state.isFinished){
                showWorkFinished()
                val outputImageUri = workInfo.outputData.getString(KEY_IMAGE_URI)
                if (!outputImageUri.isNullOrEmpty()){
                    if (userViewModel.imageUri !=null){
                        userViewModel.setOutputUri(Utils.uriOrNull(outputImageUri))
                    }else{userViewModel.cancelWork()}
//                    userViewModel.outputUri.let {
//                        binding.imageProfile.setImageURI(it)
//                    }
                    userViewModel.outputUri?.let {
                        userViewModel.setImageString(Utils.bitMapToString(MediaStore.Images.Media.getBitmap(
                            requireContext().contentResolver,
                            it
                        )))
                    }
//                    userViewModel.setImageString(Utils.bitMapToString(MediaStore.Images.Media.getBitmap(
//                        requireContext().contentResolver,
//                        Utils.uriOrNull(outputImageUri)
//                    )))
                }
            }else{
                showWorkInProgress()
            }
        }
    }
    private fun showWorkInProgress() {
        with(binding) {
            progressBar.visibility = View.VISIBLE
            button2.visibility = View.VISIBLE
            buttonUpload.visibility = View.GONE
        }
    }
    private fun showWorkFinished() {
        with(binding) {
            progressBar.visibility = View.GONE
            button2.visibility = View.GONE
            buttonUpload.visibility = View.VISIBLE
        }
    }

}