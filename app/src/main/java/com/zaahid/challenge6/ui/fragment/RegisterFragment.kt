package com.zaahid.challenge6.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.zaahid.challenge6.R
import com.zaahid.challenge6.data.local.database.entity.UserEntity
import com.zaahid.challenge6.databinding.FragmentRegisterBinding
import com.zaahid.challenge6.ui.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterFragment : Fragment() {
    private lateinit var binding: FragmentRegisterBinding
    private val userViewModel : UserViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentRegisterBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.button.setOnClickListener {
            cekForm()
        }
    }

    private fun cekForm() {
        if (binding.textinputusername.text.toString().isEmpty().not()) {
            if (binding.textinputpassword.text.toString().isEmpty().not()) {
                if (binding.inputConfirmPassword.text.toString().isEmpty().not()) {
                    if (binding.textinputalamat.text.toString().isEmpty().not()) {
                        register()
                    } else Toast.makeText(
                        requireContext(),
                        getString(R.string.empty_address),
                        Toast.LENGTH_SHORT
                    ).show()
                } else Toast.makeText(
                    requireContext(),
                    getString(R.string.confirm_pass_empty),
                    Toast.LENGTH_SHORT
                ).show()
            } else Toast.makeText(requireContext(), getString(R.string.password_is_empty), Toast.LENGTH_SHORT)
                .show()
        } else Toast.makeText(requireContext(), getString(R.string.username_is_empty), Toast.LENGTH_SHORT)
            .show()
    }

    private fun register() {
        if (cekPasswordIsSame()) {
            userViewModel.insertUser(parseFormToEntity())
            Toast.makeText(requireContext(), getString(R.string.successfully_insert_data), Toast.LENGTH_LONG)
                .show()
            findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        } else Toast.makeText(requireContext(), getString(R.string.password_not_same), Toast.LENGTH_LONG).show()

    }

    private fun parseFormToEntity(): UserEntity {
        return UserEntity(
            username = binding.textinputusername.text.toString().trim(),
            useremail = binding.textinputemail.text.toString().trim(),
            userpassword = binding.textinputpassword.text.toString().trim(),
            alamat = binding.textinputalamat.text.toString().trim()
        )
    }

    fun cekPasswordIsSame(): Boolean {
        return binding.textinputpassword.text.toString()
            .trim() == binding.inputConfirmPassword.text.toString().trim()
    }
}