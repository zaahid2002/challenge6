package com.zaahid.challenge6.ui.fragment.homefragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.zaahid.challenge6.R
import com.zaahid.challenge6.databinding.FragmentPopularHomeBinding
import com.zaahid.challenge6.ui.UserViewModel
import com.zaahid.challenge6.wrapper.Resource
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class PopularHomeFragment : Fragment() {

    private var _binding: FragmentPopularHomeBinding?=null
    private val binding get() =_binding!!
    private val userViewModel : UserViewModel by viewModels()

    private val adapter:ItemAdapter by lazy {
        ItemAdapter{
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.fragment_container,ItemFragment(it))
                ?.addToBackStack(null)
                ?.commit()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding  = FragmentPopularHomeBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initList(view)
        observeData()
        userViewModel.getUserName().observe(viewLifecycleOwner){
            val a = getString(R.string.welcome)+" "+it
            binding.textWelcomeUser.text = a
        }
        userViewModel.getLang().observe(viewLifecycleOwner){
            userViewModel.popularMovie(it)
        }
    }

    private fun observeData() {
        userViewModel.listMovieResult.observe(viewLifecycleOwner){
            when(it){
                is Resource.Empty -> {}
                is Resource.Error -> {
                    Toast.makeText(context, "error", Toast.LENGTH_LONG).show()
                }
                is Resource.ErrorTrow -> {}
                is Resource.Loading -> {
                    binding.rvPopular.visibility = View.GONE
                    binding.pbPopular.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.rvPopular.visibility = View.VISIBLE
                    binding.pbPopular.visibility = View.GONE
                    it.payload?.let { it1 -> adapter.submitData(it1) }
                }
            }
        }
    }
    private fun initList(view: View) {
        binding.rvPopular.apply {
            layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL,false)
            adapter = this@PopularHomeFragment.adapter
        }
    }
}