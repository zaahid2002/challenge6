package com.zaahid.challenge6.ui.fragment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.zaahid.challenge6.R
import com.zaahid.challenge6.databinding.ActivityHomeBinding
import com.zaahid.challenge6.ui.UserViewModel
import com.zaahid.challenge6.ui.fragment.homefragment.PopularHomeFragment
import com.zaahid.challenge6.ui.fragment.homefragment.ProfileHomeFragment
import com.zaahid.challenge6.ui.fragment.homefragment.SearchHomeFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding
    private val userViewModel : UserViewModel by viewModels()
    private val fragmentManager = supportFragmentManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.bottomNavigation.setOnItemSelectedListener { item ->
            when(item.itemId){
                R.id.popular_movie-> {
                    fragmentManager.commit {
                        replace<PopularHomeFragment>(R.id.fragment_container)
                    }
                    true
                }
                R.id.search_movie->{

                    fragmentManager.commit {
                        replace<SearchHomeFragment>(R.id.fragment_container)
                    }
                    true
                }
                R.id.profile->{

                    fragmentManager.commit {
                        replace<ProfileHomeFragment>(R.id.fragment_container)
                    }
                    true
                }
                else->false
            }
        }
    }
}