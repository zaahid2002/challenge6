package com.zaahid.challenge6.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zaahid.challenge6.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {
    private lateinit var binding:FragmentHomeBinding
//    private val userViewModel : UserViewModel by viewModelFactory {
//        UserViewModel(ServiceLocator.provideLocalRepository(requireContext()))
//    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding= FragmentHomeBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        val transaction = fragmentManagerChild.beginTransaction()
//        fragmentManagerChild.commit {
//            add<PopularHomeFragment>(R.id.fragment_container)
//        }
//        binding.bottomNavigation.setOnItemSelectedListener { item ->
//            when(item.itemId){
//                R.id.popular_movie-> {
//                    fragmentManagerChild.commit {
//                        replace<PopularHomeFragment>(R.id.fragment_container)
//                    }
//                    true
//                }
//                R.id.search_movie->{
//                    fragmentManagerChild.commit {
//                        replace<SearchHomeFragment>(R.id.fragment_container)
//                    }
//                    true
//                }
//                R.id.profile->{
//                    fragmentManagerChild.commit {
//                        replace<ProfileHomeFragment>(R.id.fragment_container)
//                    }
//                    true
//                }
//                else->false
//            }
//        }
    }

}